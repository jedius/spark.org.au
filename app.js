"use strict";

/**
 * Module dependencies.
 */
var config = require("./config");
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var layoutParams = require('./routes/middleware/layout-params');
var stylus = require('stylus');
var nib = require('nib');
var passport = require('passport');
var flash = require('connect-flash');
var path = require('path');


// if you don't want to use redis locally then comment out the following lines
var RedisStore = require('connect-redis')(express);
config.sessions.store = new RedisStore(config.redis);


module.exports.getConfiguredApp = function() {

    var app = express();

    function compileCSS(str, filepath) {
      return stylus(str)
        .set('filename', filepath)
        .set('compress', true)
        .use(nib());
    }

    var publicDirPath = path.join(__dirname, 'public');
    var sharedDirPath = path.join(__dirname, 'shared');

    app.configure(function() {
        app.set('port', config.port);
        app.use(express.logger('dev'));
        app.use(express.compress());

        // set the view engine to jade
        app.set('views', __dirname + '/views');
        app.set('view engine', 'jade');

        // will serve the favicon from memory
        app.use(express.favicon());
    });

    // in prod we have the static file server
    // run before the css generator. this is so
    // that the if the css file exists then it will
    // be served. On dev we want to force the compile
    // every single time so we serve the CSS first
    app.configure("production", function() {
        // if we're in production then static resources expire after a day
        // also, we serve static content before we do anything with cookies
        app.use(express["static"](publicDirPath, { maxAge: 86400000 }));
        app.use(express["static"](sharedDirPath, { maxAge: 86400000 }));

        // more on configuring stylus here
        // https://github.com/LearnBoost/stylus/blob/master/docs/middleware.md
        app.use(stylus.middleware({
            src: publicDirPath,
            compile: compileCSS
        }));
    });

    app.configure("development", function() {
        // more on configuring stylus here
        // https://github.com/LearnBoost/stylus/blob/master/docs/middleware.md
        app.use(stylus.middleware({
            src: publicDirPath,
            compile: compileCSS,
            force: true
        }));
        app.use(express["static"](publicDirPath));
        app.use(express["static"](sharedDirPath));
    });

    app.configure(function() {
        app.use(express.bodyParser());
        app.use(express.cookieParser());
        app.use(express.cookieSession(config.sessions));
        //app.use(passport.initialize());
        //app.use(passport.session());
        app.use(flash());
        app.use(express.csrf());
        app.use(layoutParams());
        app.use(app.router);
    });

    // TODO: make a suitable error handler
    app.configure('development', function(){
      app.use(express.errorHandler());
    });

    routes.setup(app);

    return app;
};
