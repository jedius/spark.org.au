/*
 * The purpose of this file is to abstract away the database functionality.
 * If the database is abstracted away then we can mock it in any lib that 
 * accesses it. At least that's the theory. -- Sugendran
 */
var config = require("../config.js");
var nano = require('nano')(config.couchdb);
var fs = require("fs");
var deepEquals = require('deep-equal');

// this var is a reference to the db object
var sparkdb;
// this var is the API we will expose after we have bootstrapped
var api = { };
var readyCallbacks = [];
var isReady = false;
var isBootstrapping = false;

/**
 * Topics represent an aspect of the API.
 * For each topic, you are required to add:
 * 1. [topic]-view.js - a CouchDB view 
 * 2. [topic].js - an api module that exports a function that returns the API when passed the sparkdb {function(db) : api}.
 * Your topic API will be exposed as api[topic]
 */
var topics = [
    'user',
    'project',
    'changemaker',
    'salesforce'
];

function getView(name) {
    "use strict";
    return require("./db/" + name + "-view.js");
}

var views = topics.map(getView);
function updateViews(err) {
    "use strict";
    if(err) {
        console.log("Error updating views");
        throw err;
    }
    if(views.length === 0) {
        isReady = true;
        readyCallbacks.forEach(function(callback) {
            callback(api);
        });
        return;
    }
    var view = views.pop();
    // console.log(view);
    var designId = '_design/' + view._id;
    sparkdb.get(designId, function(err, body) {
        if(err && err.message !== "missing") {
            console.log("Error getting view");
            throw err;
        }
        var params = { doc_name: designId };
        if(body) {
            view._rev = body._rev;
            if(deepEquals(view, body)) {
                return updateViews();
            }
        }
        sparkdb.insert(view, params, updateViews);
    });
}

// boot strapper
function bootstrap() {
    "use strict";
    isBootstrapping = true;

    nano.db.create('spark', function(err) {
        if (err && err.error !== 'file_exists') {
            console.log('Err: ', err);
        }

        // note we ignore the error since it's 
        // probably the database already exists
        // (not sure if this is the right way)
        sparkdb = nano.use('spark');

        topics.forEach(function(topic) {
            api[topic] = require("./db/" + topic)(sparkdb);
        });
        api.donation = require("./db/donation")(sparkdb);
        updateViews();
    });
}

// don't do anything until we've bootstrapped the database
module.exports.ready = function(callback) {
    "use strict";
    if(isReady) {
        return callback(api);
    }
    readyCallbacks.push(callback);
    if(!isBootstrapping) {
        bootstrap();
    }
};
