module.exports = function(sparkdb) {
    "use strict";

    function saveDonationInfo(donationInfo, callback) {
        if(!donationInfo.type || donationInfo.type !== 'donationInfo') {
            return callback(new Error("donation.type is not set correctly."));
        }
        if(!donationInfo._id) {
            return callback(new Error("donation._id is not set"));
        }
        donationInfo.modified_date = Date.now();
        console.log("saving donation info %j", donationInfo);
        sparkdb.insert(donationInfo, donationInfo._id, function(err, body) {
            if(err) {
                return callback(err);
            }
            donationInfo._rev = body.rev;
            callback(null, donationInfo);
        });
    }

    function getDonationInfo(donationId, callback) {
        sparkdb.get(donationId, callback);
    }

    return {
        get : getDonationInfo,
        save: saveDonationInfo
    };
};
