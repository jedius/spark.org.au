"use strict";

var requiredFields = [
    'project_slug',
    'fund_amount',
    'first_name',
    'last_name',
    'email',
    'number',
    'cvv',
    'month',
    'year',
    'address1',
    'city',
    'country',
    'state',
    'postcode',
    'anonymity'
];
var optionalFields = [
    'address2',
    'updates'
];
var allFields = requiredFields.concat(optionalFields);

function validate(req, next) {
    var actualFields = Object.keys(req.body);
    var missingFields = requiredFields.filter(function(field) { return !req.body[field]; });
    var excessFields = actualFields.filter(function(field) { return !~allFields.indexOf(field); });

    var hasInvalidFields = false;
    var invalidFieldErrors = {};
    function validateField(fieldName, getErrors) {
        if (~missingFields.indexOf(fieldName)) {
            return;
        }

        var errors = getErrors(fieldName);
        if (errors) {
            hasInvalidFields = true;
            invalidFieldErrors[fieldName] = errors;
        } 
    }
    function validateNonZeroWholeNumber(humanName, fieldName) {
        if (!/^\d+$/.test(req.body[fieldName])) {
            return humanName + ' is not a number.';
        }

        req.body[fieldName] = Number(req.body[fieldName]);
        if (req.body[fieldName] === 0) {
            return humanName + ' cannot be zero.';
        }
    }
    function validateNonblankString(humanName, fieldName) {
        if (typeof req.body[fieldName] !== 'string') {
            return humanName + ' must be a string.';
        } else if (/^\s*$/.test(req.body[fieldName])) {
            return humanName + ' cannot be blank';
        }
    }

    validateField('fund_amount', validateNonZeroWholeNumber.bind(null, 'Fund amount'));
    validateField('first_name', validateNonblankString.bind(null, 'First name'));
    validateField('last_name', validateNonblankString.bind(null, 'Last name'));
    validateField('address1', validateNonblankString.bind(null, 'Address'));
    validateField('city', validateNonblankString.bind(null, 'City'));
    validateField('country', validateNonblankString.bind(null, 'Country'));
    validateField('state', validateNonblankString.bind(null, 'State'));
    validateField('postcode', validateNonblankString.bind(null, 'Postcode'));
    validateField('anonymity', validateNonblankString.bind(null, 'Anonymity'));

    function coerceToArray(fieldName) {
        if (!req.body[fieldName]) {
            req.body[fieldName] = [];
        } else if (typeof req.body[fieldName] === 'string') {
            req.body[fieldName] = [ req.body[fieldName] ];
        }
    }
    coerceToArray('updates');

    if (missingFields.length || excessFields.length || hasInvalidFields) {
        return next({
            missing : missingFields,
            excess : excessFields,
            invalid : invalidFieldErrors
        });
    }

    next();
}

module.exports =  validate;
