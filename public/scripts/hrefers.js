$('.read-full-story').click(function(e){
    $(e.currentTarget).hide();
    $(e.currentTarget.parentElement).find('.hide-full-story').show();
    $(e.currentTarget.parentElement.parentElement.parentElement.parentElement).find('.full-story').show();
});

$('.hide-full-story').click(function(e){
    $(e.currentTarget).hide();
    $(e.currentTarget.parentElement).find('.read-full-story').show();
    $(e.currentTarget.parentElement.parentElement.parentElement.parentElement).find('.full-story').hide();
});

$('.employment').click(function(){
    document.getElementById('EMPLOYMENT').style.display='inline';
    document.getElementById('read-mill-employment').style.display='none';
    document.getElementById('hide-mill-employment').style.display='block';
});

$('.environment').click(function(){
    document.getElementById('ENVIRONMENT').style.display='inline';
    document.getElementById('read-mill-environment').style.display='none';
    document.getElementById('hide-mill-environment').style.display='block';
});

$('.equality').click(function(){
    document.getElementById('GENDER EQUALITY').style.display='inline';
    document.getElementById('read-mill-gender-equality').style.display='none';
    document.getElementById('hide-mill-gender-equality').style.display='block';
});

$('.health').click(function(){
    document.getElementById('HEALTH').style.display='inline';
    document.getElementById('read-mill-health').style.display='none';
    document.getElementById('hide-mill-health').style.display='block';
});
