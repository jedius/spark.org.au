"use strict";
/*
 * GET home page.
 */
var projectRoutes = require("./project");
var userRoutes = require("./user");
var authRoutes = require("./auth");
var express = require('express');

var db = require("../lib/db");

var oneMillionData = require("../content/oneMillion.js").oneMillion;

function renderIndexPage(req, res, next) {
    var viewBag = {
        title: "Spark* - 1 Million People out of Extreme Poverty",
        atHome: true
    };
    res.render('index', viewBag);
}
module.exports.index = renderIndexPage;

function renderLoginPage(req, res, next) {
    res.render('login');
}
module.exports.login = renderLoginPage;

function renderStaticPrivacyPolicy(req, res, next) {
    "use strict";
    var viewBag = {
        title: "Spark* Privacy Policy"
    };
    res.render('privacyPolicy', viewBag);
}

function renderStaticTermsConditions(req, res, next) {
    "use strict";
    var viewBag = {
        title: "Spark* Terms and Conditions"
    };
    res.render('termsConditions', viewBag);
}

function renderSparkInfoPage(req, res, next) {
    var viewBag = {
        title: "Spark*"
    };
    res.render('spark', viewBag);
}

function renderSupporterInfoPage(req, res, next) {
    var viewBag = {
        title: "You",
        messages: oneMillionData.message
    };
    res.render('you', viewBag);
}

function renderOneMillionPage(req, res, next) {
    db.ready(function(api) {
        api.project.search({
            order: 'creationDateDesc',
        }, function(err, result) {
            if (err) {
                return next();
            }
            var projects = [];
            projects.push({
                "_id": "a00O0000001z3laIAA",
                "_rev": "5-ababc244262a6a2e099f842d5b239691",
                "type": "project",
                "salesforce_ref": "",
                "name": "Seeds of Hope",
                "slug": "seeds-of-hope",
                "created_date": 1365194445000,
                "modified_date": 1366871250074,
                "category": "Education",
                "status": "prelaunch",
                "mission": "To provide every child in Waknam village with the support they need to access a complete education and move out of poverty.",
                "problem": "Many families in Waknam village are unable to afford the school fees to provide their children with an education.\r\nThe prices that local people are getting for their harvests each year continue to fall, and it is clear that access to better markets is needed. With these diminishing incomes only 1 in every 20 children is currently attending school.",
                "solution": "Seeds of Hope is a farming company that grows sweet potatoes. With more farmers involved, they can create bigger harvests, and justify the costs of transporting their crops to better markets. The profits from their harvests help the farmers improve their standards of living and invest in an education trust, paying the school fees of Waknam village's most vulnerable youths.",
                "people": "",
                "funding_grants": [{
                    "salesforce_ref": "a0RO0000002jeh0MAA",
                    "attained_date": null,
                    "created_date": "2013-04-05T20:44:23.000+0000",
                    "amount_usd": 1000,
                    "contributions": [],
                    "received_usd": 5150
                }],
                "followers": [],
                "messages": [],
                "help_offers": [],
                "media": {},
                "updates": [],
                "changemaker_id": "003O000000DFo4jIAD",
                "location": "Papua New Guinea",
                "published": true,
                "changemaker": {
                    "_id": "003O000000DFo4jIAD",
                    "_rev": "5-b868631219a8b57a45d5f81c22708476",
                    "type": "changemaker",
                    "salesforce_ref": "",
                    "first_name": "John",
                    "last_name": "Taka",
                    "story": "John is the only individual to ever graduate from his village, a rural community in the highlands of Papua New Guinea. He is now working as a young accountant in Port Moresby, but in his spare time he is doing all he can to help the youths of his village access the same education that he has received.",
                    "accelerator_id": null,
                    "media": {}
                },
                "searchImageUrl": '/images/projects/seeds-of-hope/search.jpg',
                "grant_info": {
                    "percent_attained": 100,
                    "amount_usd": 100
                }
            }); 
            var viewBag = {
                title: "1 Million People Out of Poverty",
                projects: projects,
                data: oneMillionData
            };
            res.render('oneMillion', viewBag);
        });
    });

}

function notImplementedRoute(req, res, next) {
    return next(new Error("This route is not implemented"));
}

function redirectToFilterPage(req, res, next) {
    res.redirect("/change-maker");
}

function redirectToHttps(req, res, next) {
    if(req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] != 'https') {
        res.redirect('https://www.sparkinternational.org'+req.url);
    } else {
        next();
    }
}

function setupRoutes(app) {
    // app.get("*", redirectToHttps);
    app.get("/", renderIndexPage);
    //app.get("/login", renderLoginPage);
    app.get("/spark", renderSparkInfoPage);
    app.get("/you", renderSupporterInfoPage);
    app.get("/1-million-people", renderOneMillionPage);
    app.get("/change-maker", projectRoutes.list);
    app.get("/change-maker/country", redirectToFilterPage);
    app.get("/change-maker/country/:countrySlug", projectRoutes.listByCountry);
    app.get("/change-maker/category", redirectToFilterPage);
    app.get("/change-maker/category/:categorySlug", projectRoutes.listByCategory);
    app.get("/change-maker/country/:countrySlug/:categorySlug", projectRoutes.listByCountryAndCategory);
    app.get("/change-maker/:projectSlug", projectRoutes.getProject);
    app.post("/change-maker/:projectSlug/donate", projectRoutes.donation.donate);
    app.get("/supporter/:userId", notImplementedRoute, userRoutes.userProfile);
    app.get("/privacy-policy", renderStaticPrivacyPolicy);
    app.get("/terms-and-conditions", renderStaticTermsConditions);

    // authentication routes
    require("./auth").addAuthRoutes(app);
}
module.exports.setup = setupRoutes;
