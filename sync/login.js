var nforce = require('nforce');
var config = require("../config");

for(var key in config.salesforce) {
    if(config.salesforce[key] === 'notset') {
        throw new Error("invalid salesforce config");
    }
}

var sf = nforce.createConnection(config.salesforce);

module.exports.connect = function(callback) {
    sf.authenticate({
        username: config.salesforce.username,
        password: config.salesforce.password,
        securityToken: config.salesforce.securityToken
    }, function(err, resp) {
      if(err) {
        console.error('nforce: unable to authenticate to salesforce');
        console.error(err);
        callback(err);
      } else {
        console.log('nforce: authenticated!');
        callback(null, {
            oauth: resp, nforce: sf
        });
      }
    });
};

module.exports.nforce = nforce;
